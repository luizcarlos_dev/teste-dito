
export const OK_RESPONSE = {
    code: 200,
    message: 'success'
}

export const CREATED_RESPONSE = {
    code: 201,
    message: 'success_on_create'
}

export const UPDATED_RESPONSE = {
    code: 200,
    message: 'success_on_update'
}

export const DELETED_RESPONSE = {
    code: 200,
    message: 'success_on_delete'
}

export const INCORRECT_REQUEST_RESPONSE = {
    code: 400,
    message: 'incorrect_request'
}

export const UNAUTHORIZED_RESPONSE = {
    code: 401,
    message: 'unauthorized'
}

export const FORBIDDEN_RESPONSE = {
    code: 403,
    message: 'forbidden'
}

export const NOT_FOUND_RESPONSE = {
    code: 404,
    message: 'fail_on_find'
}

export const ROUTE_NOT_FOUND_RESPONSE = {
    code: 404,
    message: 'route_not_found'
}

export const INTERNAL_SERVER_ERROR_RESPONSE = {
    code: 500,
    message: 'internal_server_error'
}

export const FOUND_RESPONSE = {
    code: 302,
    message: 'success_on_find'
}