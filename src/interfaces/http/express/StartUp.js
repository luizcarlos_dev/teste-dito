import express from 'express'
import path from 'path'
import fs from 'fs'
import { CustomResponse } from './CustomResponse'
import { ROUTE_NOT_FOUND_RESPONSE, OK_RESPONSE } from './constansts'
import Security from './Security'
import bodyParser from 'body-parser'
import http from 'http'
// import https from 'https'
import swaggerJSDoc from 'swagger-jsdoc'

const envName = process.env.NODE_ENV
const env = require('../../../env/' + envName + '.env.js').env

const expressApp = express()

export class StartUp {

    /**
     * @param {object} customCorsOptions - configurações customizadas de CORS em formato de objeto
     * @returns {void}
     * @public
     * @public
     */
    static setupCors(customCorsOptions) {

        const options = customCorsOptions || {
            'x-powered-by': env.app.name,
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        }

        Object.keys(options).forEach((header) => {

            expressApp.use((req, res, next) => {
                res.header(header, options[header])
                next()
            })

        })

    }

    /**
     * Define os endpoints da API
     * @private
     */
    static _setupRouters() {

        expressApp.use(bodyParser.json())

        fs.readdirSync(path.join(__dirname, './routers'))
            .forEach((module) => {
                if (!module.includes('.js.map')) {
                    require(path.join(__dirname, `../express/routers/${module}`)).default(expressApp)
                }
            })

        const swaggerApiVersion = 'v1'

        const swaggerSpec = swaggerJSDoc({
            swaggerDefinition: {
                // openapi: '3.0.0',
                swagger: '2.0',
                info: {
                    title: 'Collector API',
                    // swagger: '2.0',
                    version: swaggerApiVersion,
                    description: 'Api de exemplo'
                },
                basePath: '/'
            },
            apis: ['src/interfaces/http/express/routers/**.js']
        })

        expressApp.get('/', (req, res) => {
            CustomResponse.send(res, { status: 'API working' }, OK_RESPONSE)
        })

        expressApp.get(`/api-docs/${swaggerApiVersion}/swagger.json`, (req, res) => {
            CustomResponse.send(res, swaggerSpec, OK_RESPONSE)
        })

        expressApp.get('*', (req, res) => {
            CustomResponse.send(res, ROUTE_NOT_FOUND_RESPONSE.message, ROUTE_NOT_FOUND_RESPONSE)
        })

        expressApp.emit('ApiStarted')

    }

    /**
     * Inicia a da API
     * @param {object} ssl - credenciais de ssl
     * @returns {void}
     * @public
     */
    static start(ssl) {

        StartUp._setupRouters()

        const security = new Security()

        security.makeSecure(expressApp)

        // const serverExpress = env.server.secure && ssl.cert ? https.createServer(ssl, expressApp) : http.createServer(expressApp)

        const serverExpress = http.createServer(expressApp)

        serverExpress.listen(env.server.port, env.server.host, StartUp._expressListenCallback(ssl))

    }

    /**
     * Retorno na inicialização do servidor express
     * @param {object} ssl - credenciais de ssl
     * @returns {void}
     * @public
     */
    static _expressListenCallback(ssl) {

        // eslint-disable-next-line no-console
        console.log(`\n${env.app.name} on at ${env.server.host}:${env.server.port}\n`)

        if (env.server.secure && ssl.cert) {
            // eslint-disable-next-line no-console
            console.log('[SSL_ON]\tSecure')
        } else {
            // eslint-disable-next-line no-console
            console.log('[SSL_OFF]\tNOT SECURE (!)')
        }
    }

}