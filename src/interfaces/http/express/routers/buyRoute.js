import { OK_RESPONSE } from '../constansts'
import { CustomResponse } from '../CustomResponse'
import { BuyAplication } from '../../../../app/BuyAplication'

/**
 * MiddleWare que obtem os navegaçãos
 * @private
 */
const _getBuyMiddleWare = async (req, res) => {
    CustomResponse.send(res, await new BuyAplication().get(), OK_RESPONSE)
}

/**
 * Endpoints para o recurso navegação
 * @public
 */
const BuyRoutes = (route) => {
    route.get('/api/buy/timeline', _getBuyMiddleWare)
}

export default BuyRoutes