import { CREATED_RESPONSE, OK_RESPONSE } from '../constansts'
import { CustomResponse } from '../CustomResponse'
import { NavigationAplication } from '../../../../app/NavigationAplication'

/**
 * MiddleWare que obtem os navegaçãos
 * @private
 */
const _getNavigationMiddleWare = async (req, res) => {
    const where = req.query.where ? JSON.parse(req.query.where) : {}

    CustomResponse.send(res, await new NavigationAplication().get(where), OK_RESPONSE)
}

/**
 * MiddleWare que cria uma novo navegação
 * @private
 */
const _createNavigationMiddleWare = async (req, res) => {
    const body = req.body

    CustomResponse.send(res, await new NavigationAplication().create(body), CREATED_RESPONSE)
}

/**
 * Endpoints para o recurso navegação
 * @public
 */
const NavigationRoutes = (route) => {
    route.get('/api/navigation', _getNavigationMiddleWare)
    route.post('/api/navigation', _createNavigationMiddleWare)
}

export default NavigationRoutes