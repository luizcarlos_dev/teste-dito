import { CREATED_RESPONSE, OK_RESPONSE } from '../constansts'
import { CustomResponse } from '../CustomResponse'
import { UserAplication } from '../../../../app/UserAplication'

/**
 * MiddleWare que obtem os usuários
 * @private
 */
const _getUserMiddleWare = async (req, res) => {
    const where = req.query.where ? JSON.parse(req.query.where) : {}

    CustomResponse.send(res, await new UserAplication().get(where), OK_RESPONSE)
}

/**
 * MiddleWare que cria uma novo usuário
 * @private
 */
const _createUserMiddleWare = async (req, res) => {
    const body = req.body

    CustomResponse.send(res, await new UserAplication().create(body), CREATED_RESPONSE)
}

/**
 * Endpoints para o recurso usuário
 * @public
 */
const UserRoutes = (route) => {
    route.get('/api/user', _getUserMiddleWare)
    route.post('/api/user', _createUserMiddleWare)
}

export default UserRoutes