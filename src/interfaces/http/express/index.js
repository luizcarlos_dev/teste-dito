import {SetUpSequelize} from '../../../infra/SetUpSequelize'
import {StartUp} from './StartUp'
import SSL from '../../../infra/SSL'

SetUpSequelize.start()

const ssl = new SSL().getCredentials()

StartUp.setupCors(null)
StartUp.start(ssl)