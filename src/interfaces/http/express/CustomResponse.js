export class CustomResponse {

    /**
     * responsta customizada
     * @param res
     * @param data
     * @param ResponseType
     * @param customMessage
     */
    static send(res, data, ResponseType, customMessage = null) {
        res.status(ResponseType.code).json(
            {
                code: ResponseType.code,
                data: data,
                message: customMessage ? customMessage : ResponseType.message
            }
        )
    }

}