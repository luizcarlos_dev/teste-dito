import { createContainer, InjectionMode, asClass } from 'awilix'
import { UserRepository } from './domain/repositories/UserRepository'
import { NavigationRepository } from './domain/repositories/NavigationRepository'
import { BuyService } from './domain/servicies/BuyService'

const container = createContainer({
    injectionMode: InjectionMode.PROXY
})

container
    .register({
        UserRepository: asClass(UserRepository),
        NavigationRepository: asClass(NavigationRepository),
        BuyService: asClass(BuyService)
    })

export default container