
import { BaseCrudApplication } from './BaseCrudApplication'

export class NavigationAplication extends BaseCrudApplication {

    /**
      * Método construtor
      * @returns {void}
      * @public
      */
    constructor() {
        const container = require('../container').default

        const repository = container.resolve('NavigationRepository')

        super(repository)
    }

    /**
     * Método que obtem os usuários
     * @param {object} where - Filtro
     * @returns {Navigation[]}
     * @public
     */
    get(where) {
        return super.get(where)
    }

    /**
     * Método que obtem o navegação por id
     * @param {number} navigationId - Id da navegação
     * @returns {Navigation}
     * @public
     */
    // eslint-disable-next-line no-unused-vars
    getById(navigationId) {
        return new Error('Not implemented')
    }

    /**
    * Método que cria um novo navegação
    * @param {object} navigation - novos dados da navegação que será criada
    * @returns {Navigation}
    * @public
    */
    create(navigation) {
        return super.create(navigation)
    }

    /**
    * Método que atualiza um navegação
    * @param {number} navigationId - Id da navegação
    * @param {object} navigation - dados da navegação que será atualizado
    * @returns {Navigation}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    update(navigationId, navigation) {
        return new Error('Not implemented')
    }

    /**
    * Método que remove um navegação
    * @param {number} navigationId - Id da navegação
    * @returns {boolean}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    delete(navigationId) {
        return new Error('Not implemented')
    }

}