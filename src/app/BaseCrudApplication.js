export class BaseCrudApplication {

    /**
     * Método construtor
     * @param {CrudRepoitory} repository - Repositório
     * @returns {void}
     * @public
     */
    constructor(repository) {
        this.repository = repository
    }

    /**
     * Método que obtem o model
     * @param {object} where - Filtro
     * @returns {any[]}
     * @public
     */
    get(where) {
        return this.repository.findAll(where)
    }

    /**
     * Método que obtem o modelo por id
     * @param {number} id - Id do modelo
     * @returns {any}
     * @public
     */
    getById(id) {
        return this.repository.findById(id)
    }

    /**
    * Método que cria o modelo
    * @param {object} model - dados do modelo que será criada
    * @returns {any}
    * @public
    */
    create(model) {
        return this.repository.create(model)
    }

    /**
   * Método que atualiza o modelo
   * @param {number} modelId - Id do modelo
   * @param {object} model - novos dados do modelo que será atualizado
   * @returns {any}
   * @public
   */
    update(modelId, model) {
        return this.repository.update(modelId, model)
    }

    /**
    * Método que remove um model
    * @param {number} modelId - Id do model
    * @returns {boolean}
    * @public
    */
    delete(modelId) {
        return this.repository.delete(modelId)
    }

}