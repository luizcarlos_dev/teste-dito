export class BuyAplication {

    /**
      * Método construtor
      * @returns {void}
      * @public
      */
    constructor() {
        const container = require('../container').default

        this.service = container.resolve('BuyService')
    }

    /**
     * Método que obtem as compras
     * @returns {Buy[]}
     * @public
     */
    async get() {

        const data = await this.service.get()

        const events = data.events

        // Obter ids das transções
        const transactionIds = events.map(event =>
            event.custom_data.find(item =>
                item.key === 'transaction_id'
            ).value
        )

        // Obter ids distintos das transções
        const uniqueTransactionIds = transactionIds
            .filter((value, index, self) => self.indexOf(value) === index)

        // Modelar array de objetos a partir das transações
        const buys = uniqueTransactionIds.map(transactionId => {

            // Obter compras associadas a determinada transação
            const buy = events.filter(event =>
                event.custom_data.find(item =>
                    item.key === 'transaction_id'
                ).value === transactionId &&
                event.event === 'comprou'
            )

            // Obter produtos associados a determinada transação
            const products = events.filter(event =>
                event.custom_data.find(item =>
                    item.key === 'transaction_id'
                ).value === transactionId &&
                event.event === 'comprou-produto'
            )

             // Modelar retorno
            return {
                timestamp: buy[0].timestamp,
                revenue: buy[0].revenue,

                'transaction_id': buy[0].custom_data.find(item =>
                    item.key === 'transaction_id'
                ).value,

                'store_name': buy[0].custom_data.find(item =>
                    item.key === 'store_name'
                ).value,

                products: products.map(product => {
                    return {
                        name: product.custom_data.find(item =>
                            item.key === 'product_name'
                        ).value,
                        price: product.custom_data.find(item =>
                            item.key === 'product_price'
                        ).value
                    }
                })
            }
        })

        // Ordenar pelo timestamp de forma decrescente
        const buysSorted = buys.reverse(buy => buy.timestamp)

        return { timeline: buysSorted }
    }

}