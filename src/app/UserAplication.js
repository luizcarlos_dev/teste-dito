
import { BaseCrudApplication } from './BaseCrudApplication'

export class UserAplication extends BaseCrudApplication {

    /**
      * Método construtor
      * @returns {void}
      * @public
      */
    constructor() {
        const container = require('../container').default

        const repository = container.resolve('UserRepository')

        super(repository)
    }

    /**
     * Método que obtem os usuários
     * @param {object} where - Filtro
     * @returns {User[]}
     * @public
     */
    get(where) {
        return super.get(where)
    }

    /**
     * Método que obtem o usuário por id
     * @param {number} userId - Id do usuário
     * @returns {User}
     * @public
     */
    // eslint-disable-next-line no-unused-vars
    getById(userId) {
        return new Error('Not implemented')
    }

    /**
    * Método que cria um novo usuário
    * @param {object} user - novos dados do usuário que será criado
    * @returns {User}
    * @public
    */
    create(user) {
        return super.create(user)
    }

    /**
    * Método que atualiza um usuário
    * @param {number} userId - Id do usuário
    * @param {object} user - dados do usuário que será atualizado
    * @returns {User}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    update(userId, user) {
        return new Error('Not implemented')
    }

    /**
    * Método que remove um usuário
    * @param {number} userId - Id do usuário
    * @returns {boolean}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    delete(userId) {
        return new Error('Not implemented')
    }

}