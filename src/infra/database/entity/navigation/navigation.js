const envName = process.env.NODE_ENV
const env = require('../../../../env/' + envName + '.env.js').env
const schema = env.database.schema

const Navigation = (sequelize, dataTypes) => {

    return sequelize.define('Navigation', {

        id: {
            field: 'id',
            type: dataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            unique: true,
            required: true,
            autoIncrement: true
        },
        userId: {
            field: 'user_id',
            type: dataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            unique: false,
            required: true,
            autoIncrement: false
        },
        event: {
            field: 'event',
            type: dataTypes.STRING,
            unique: false,
            required: true
        },
        timestamp: {
            field: 'timestamp',
            type: dataTypes.DATE,
            allowNull: false
        },
        createdAt: {
            field: 'created_at',
            type: dataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            field: 'updated_at',
            type: dataTypes.DATE,
            allowNull: false
        }
    }, {
            tableName: 'navigation'
        }
    ).schema(schema)

}

export default Navigation