const envName = process.env.NODE_ENV
const env = require('../../../../env/' + envName + '.env.js').env
const schema = env.database.schema

const User = (sequelize, dataTypes) => {

    return sequelize.define('User', {
        id: {
            field: 'id',
            type: dataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            unique: true,
            required: true,
            autoIncrement: true
        },
        name: {
            field: 'name',
            type: dataTypes.STRING,
            unique: false,
            required: true
        },
        createdAt: {
            field: 'created_at',
            type: dataTypes.DATE,
            allowNull: true
        },
        updatedAt: {
            field: 'updated_at',
            type: dataTypes.DATE,
            allowNull: true
        }
    }, {
            tableName: 'user'
        }
    ).schema(schema)

}

export default User