// import response from '../core/Response';
// import request from 'request-promise';
// import ACLVerify from './ACLVerify';
//
// // Config
// import Config    from '../config/api.conf';
// const config        = new Config();
//
// class TokenVerify {
//
//     constructor() {}
//
//     /**
//      * This middleware verify in account api if received authorization is valid
//      * @param req
//      * @param res
//      * @param next
//      * @returns {*}
//      */
//     checkout(req, res, next) {
//
//         if (!req.headers.authorization) {
//             return response.send(res, null, response.UNAUTHORIZED, 'authorization_missing')
//         }
//
//         TokenVerify._requestAccountServiceApi(req.headers.authorization)
//             .then(data => {
//                 // Check auto validate in toke
//                 if (data.data.valid && data.data.type) {
//                     // If valid go to next middleware
//
//                     ACLVerify.acl.isAllowed(data.data.type, req.route.path, req.method.toLowerCase(), (err, allowed) => {
//                         if (err || !allowed) {
//                             return response.send(res, null, response.UNAUTHORIZED, 'invalid_acl_authorization');
//                         }
//                         req.jwt = data.data;
//
//                         return next()
//                     });
//                 } else {
//                     return response.send(res, data, response.UNAUTHORIZED, 'invalid_authorization')
//                 }
//             })
//             .catch(() => {
//                 return response.send(res, null, response.UNAUTHORIZED)
//             });
//     }
//
//     /**
//      * Static function to request account api
//      * @param authorization
//      * @returns {*}
//      * @private
//      */
//     static  _requestAccountServiceApi(authorization) {
//
//         // Ignore self assigned certificate
//         process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
//
//         // Request options
//         const options = {
//             method: 'POST',
//             uri: config.env.server.uriAccountsApi + '/verify',
//             json: {
//                 authorization: authorization
//             }
//         };
//
//         // Return request promise
//         return request(options);
//     }
// }
//
// export default new TokenVerify();