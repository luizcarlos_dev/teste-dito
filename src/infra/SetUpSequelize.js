import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'

const envName = process.env.NODE_ENV
const env = require('../env/' + envName + '.env.js').env

let config = {}

export class SetUpSequelize {

    /**
     * Obtem o valor da propriedade Config
     * @returns {object}
     * @public
     */
    static get Config() {
        return config
    }

    /**
     * Seta o valor da propriedade Config
     * @param {object} Config - configuração do sequelize
     * @returns {void}
     * @public
     */
    static set Config(Config) {
        config = Config
    }

    /**
     * Carrega os modelos do sequelize
     * @param {string} dirPath - caminho do modelo do sequelize
     * @param {string} dialect - dialeto do sequelize
     * @returns {void}
     * @private
     */
    static _loadModels (dirPath, dialect) {

        const models = fs.readdirSync(dirPath)

        for (const model of models) {

            const itemPath = path.join(dirPath, model)
            const itemIsDir = fs.existsSync(itemPath) && fs.lstatSync(itemPath).isDirectory()

            if (itemIsDir) {
                SetUpSequelize._loadModels(itemPath, dialect)
            } else {
                try {
                    const importedItem = SetUpSequelize.Config[dialect].sequelize.import(itemPath)

                    SetUpSequelize.Config[dialect].DB[importedItem.name] = importedItem
                } catch (error) {
                    // eslint-disable-next-line no-console
                    console.error(error)
                }
            }
        }
    }

    /**
     * Associa os modelos do sequelize
     * @param {string} dialect - dialeto do sequelize
     * @returns {promise}
     * @private
     */
    static _associateModels (dialect) {
        return Object
            .keys(SetUpSequelize.Config[dialect].DB)
            .forEach((model) => {
                if ('associate' in SetUpSequelize.Config[dialect].DB[model]) {
                    SetUpSequelize.Config[dialect].DB[model].associate(SetUpSequelize.Config[dialect].DB)
                }
            })
    }

    /**
     * Sincroniza os schemas na base de dados
     * @param {string} dialect - dialeto do sequelize
     * @returns {void}
     * @private
     */
    static _sync (dialect) {
        SetUpSequelize.Config[dialect].sequelize.sync(
            {
                force: false,
                logging: false
            }
        )
    }

    /**
     * Cria instância do sequilize
     * @param {object} database - database do sequelize
     * @returns {void}
     * @private
     */
    static _createSequelizeInstance (database) {

        const connectionString = `${database.dialect}://${database.user}:${database.pass}@${database.host}:${database.port}/${database.name}?currentSchema=${database.schema}`

        // eslint-disable-next-line no-console
        const logging = process.env.NODE_ENV === 'production' ? null : console.log

        SetUpSequelize.Config[database.dialect].sequelize = new Sequelize(
            connectionString,
            {
                schema: database.schema,
                logging: logging
            }
        )
    }

    /**
     * Cria objeto de conexão com o banco de dados
     * @returns {any}
     * @private
     */
    static async _createDialectObject() {

        const database = env.database

        const dialect = database.dialect

        SetUpSequelize.Config[dialect] = {
            sequelize: null,
            DB: []
        }

        SetUpSequelize._createSequelizeInstance(database)

        const dirPath = path.join(__dirname, 'database/entity/')

        SetUpSequelize._loadModels(dirPath, dialect)

        const associateModelsError = await SetUpSequelize._associateModels(dialect)

        SetUpSequelize._sync(dialect)

        return associateModelsError
    }

    /**
     * Inicia as configurações do Sequelize
     * @returns {void}
     */
    static async start() {
        const createDialectObjectResultError = await SetUpSequelize._createDialectObject()

        if (createDialectObjectResultError){
            // eslint-disable-next-line no-console
            console.log('[SQL Error] \n\n\t' + createDialectObjectResultError.message + '\n\tEXIT\n')
        } else {
            // eslint-disable-next-line no-console
            console.log('[SQL]\tConnection Success')
        }
    }

}