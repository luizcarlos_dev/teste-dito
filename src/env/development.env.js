export const env = {

    app: {
        name: 'Collector API - DEV MODE',
        version: '0.0.1'
    },

    server: {
        secure: true,
        host: '0.0.0.0',
        port: 3001
    },

    database: {
        dialect: 'postgres',
        host: 'localhost',
        user: 'root',
        pass: 'root',
        name: 'test-dito',
        port: 5432,
        schema: 'collector-api'
    }

}