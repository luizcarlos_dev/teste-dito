
export class User {

    constructor(item = {}) {
        Object.assign(this, {
            id: item.id,
            name: item.name
        })
    }

    get id() { return this._id }
    set id(id) { this._id = id }

    get name() { return this._name }
    set name(name) { this._name = name }

    toJSON() {
        return {
            id: this._id,
            name: this._name
        }
    }

}