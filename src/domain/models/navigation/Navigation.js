export class Navigation {

    constructor(item = {}) {
        Object.assign(this, {
            id: item.id,
            userId: item.userId,
            // user: item.user,
            event: item.event,
            timestamp: item.timestamp,
            createdAt: item.createdAt,
            updatedAt: item.updatedAt
        })
    }


    get id() { return this._id }
    set id(id) { this._id = id }

    get userId() { return this._userId }
    set userId(userId) { this._userId = userId }

    // get user() { return this.user }
    // set user(user) { this.user = user }

    get event() { return this._event }
    set event(event) { this._event = event }

    get timestamp() { return this._timestamp }
    set timestamp(timestamp) { this._timestamp = timestamp }

    toJSON() {
        return {
            id: this._id,
            userId: this._userId,
            // user: this._user,
            event: this._event,
            timestamp: this._timestamp
        }
    }
}