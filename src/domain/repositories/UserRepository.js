import { User } from '../models/user/User'
import { CrudRepository } from './CrudRepository'
import { SetUpSequelize } from '../../infra/SetUpSequelize'

const db = SetUpSequelize.Config.postgres.DB

export class UserRepository extends CrudRepository {

    /**
     * Método construtor
     * @returns {void}
     * @public
     */
    constructor() {
        super()
        this.model = db.User
    }

    /**
    * Retorna todos os usuários
    * @param {object} where - Filtro
    * @returns {User[]}
    * @public
    */
    findAll(where) {

        // eslint-disable-next-line guard-for-in
        for (const key in where) {
            const item = where[key]

            for (const itemKey in item) {
                if (itemKey === 'like') {
                    item.$like = '%' + item.like + '%'
                }
            }
        }

        return this.model.findAll({ where })
            .map(item => new User(item))
    }

    /**
    * Obter usuário por id
    * @param {number} id - Id do usuário
    * @returns {User}
    * @public
    */
    async findById(id) {
        const item = await this.model.findOne({
            where: {
                id: id
            }
        })

        return new User(item)
    }

    /**
    * Cria usuário
    * @param {object} user - dados do usuário que será criado
    * @returns {User}
    * @public
    */
    async create(user) {
        const createdUser = await this.model.create(user)

        return createdUser
    }

    /**
    * Atualiza usuário
    * @param {number} id - Id do usuário
    * @param {object} user - dados do usuário que será criado
    * @returns {User}
    * @public
    */
    async update(id, user) {
        user.id = id
        const updatedid = await this.model.update(user, { where: { id: id } })

        const updatedUser = await this.model.findOne({
            where: {
                id: updatedid
            }
        })

        return updatedUser
    }

    /**
    * Remove usuário
    * @param {number} id - Id do usuário
    * @returns {boolean}
    * @public
    */
    async delete(id) {
        const user = await this.model.findOne({
            where: {
                id: id
            }
        })

        if (user) {
            user.destroy()
        }

        return !!user

    }

}