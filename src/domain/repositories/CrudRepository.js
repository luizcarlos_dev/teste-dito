export class CrudRepository {

    /**
     * Método construtor
     * @returns {void}
     * @public
     */
    constructor() {
    }

    /**
    * Retorna todos os modelos
    * @param {object} where - Filtro
    * @returns {any[]}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    async findAll(where) { }

    /**
     * Obter modelo por id
     * @param {number} id - Id do modelo
     * @returns {any}
     * @public
     */
    // eslint-disable-next-line no-unused-vars
    async findById(id) { }

    /**
    * Cria model
    * @param {object} modelo - dados do modelo que será criado
    * @returns {any}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    async create(model) { }

    /**
    * Atualiza model
    * @param {number} modelId - Id do modelo
    * @param {object} model - dados do modelo que será criado
    * @returns {any}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    async update(modelId, model) { }

    /**
    * Remove model
    * @param {number} modelId - Id do modelo
    * @returns {boolean}
    * @public
    */
    // eslint-disable-next-line no-unused-vars
    async delete(modelId) { }

}