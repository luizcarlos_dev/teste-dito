
import { Navigation } from '../models/navigation/Navigation'
import { CrudRepository } from './CrudRepository'
import { SetUpSequelize } from '../../infra/SetUpSequelize'

const db = SetUpSequelize.Config.postgres.DB

export class NavigationRepository extends CrudRepository {

    /**
     * Método construtor
     * @returns {void}
     * @public
     */
    constructor() {
        super()
        this.model = db.Navigation
    }

    /**
    * Retorna todas os navegações
    * @param {object} where - Filtro
    * @returns {Navigation[]}
    * @public
    */
    findAll(where) {

        // eslint-disable-next-line guard-for-in
        for (const key in where) {
            const item = where[key]

            for (const itemKey in item) {
                if (itemKey === 'like') {
                    item.$like = '%' + item.like + '%'
                }
            }
        }

        return this.model.findAll({ where: where })
            .map(item => new Navigation(item))
    }

    /**
    * Obter navegação por id
    * @param {number} id - Id da navegação
    * @returns {Navigation}
    * @public
    */
    async findById(id) {
        const item = await this.model.findOne({
            where: {
                id: id
            }
        })

        return new Navigation(item)
    }

    /**
    * Cria navegação
    * @param {object} navigation - dados da navegação que será criada
    * @returns {Navigation}
    * @public
    */
    async create(navigation) {
        const createdUser = await this.model.create(navigation)

        return createdUser
    }

    /**
    * Atualiza navegação
    * @param {number} id - Id da navegação
    * @param {object} navigation - dados da navegação que será atualizada
    * @returns {Navigation}
    * @public
    */
    async update(id, navigation) {
        navigation.id = id
        const updatedid = await this.model.update(navigation, { where: { id: id } })

        const updatedUser = await this.model.findOne({
            where: {
                id: updatedid
            }
        })

        return updatedUser
    }

    /**
    * Remove navegação
    * @param {number} id - Id da navegação
    * @returns {boolean}
    * @public
    */
    async delete(id) {
        const navigation = await this.model.findOne({
            where: {
                id: id
            }
        })

        if (navigation) {
            navigation.destroy()
        }

        return !!navigation

    }

}