import rp from 'request-promise'

export class BuyService {

    /**
     * Método construtor
     * @returns {void}
     * @public
     */
    constructor() { }

    /**
     * Método que obtem as compras
     * @returns {Buy[]}
     * @public
     */
    async get() {
        const options = {
            uri: 'https://storage.googleapis.com/dito-questions/events.json',
            json: true
        }

        try {
            return await rp(options)
        } catch (err) {
            return null
        }

    }

}